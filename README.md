# isasha

## Gist

Attempt to formalize and proofs of Shannon's theorems in Isabelle, to replicate what has been done in Coq/SSReflect.

## Progress

So far, one implication out of the four has been proved. It is available on the [Archive of Formal Proofs](https://www.isa-afp.org/entries/Source_Coding_Theorem.shtml) website.

## How-to

### Parse and explore the file

Use [**Isabelle2016**](https://www.cl.cam.ac.uk/research/hvg/Isabelle) to open the theory files.

### Indentation

I ended up writing a small python script to avoid indenting my code by hand, it's really [hackish](https://github.com/hiqua/isadent). Proper auto-indentation features are under development in the newer versions of Isabelle.

## References

Proof of Shannon's theorems in Coq/SSReflect: https://staff.aist.go.jp/reynald.affeldt/shannon
